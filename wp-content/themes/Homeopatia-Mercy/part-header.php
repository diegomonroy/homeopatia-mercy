<?php
$style = '';
if ( is_front_page() ) : $style = 'home'; endif;
?>
<!-- Begin Top -->
	<section class="top" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-7 columns">
				<?php dynamic_sidebar( 'top_1' ); ?>
			</div>
			<div class="small-12 medium-2 columns">
				<?php dynamic_sidebar( 'top_2' ); ?>
			</div>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'top_3' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->
<!-- Begin Logo -->
	<section class="logo" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 medium-6 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
		</div>
	</section>
<!-- End Logo -->
<!-- Begin Menu -->
	<section class="menu_wrap <?php echo $style; ?>" data-wow-delay="0.5s">
		<div class="row collapse align-center align-middle">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Menu -->