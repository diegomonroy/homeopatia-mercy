<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php if ( is_front_page() ) : dynamic_sidebar( 'banner_inicio' ); endif; ?>
				<?php if ( is_page( array( 'sedes' ) ) ) : dynamic_sidebar( 'banner_sedes' ); endif; ?>
				<?php if ( is_page( array( 'contactenos' ) ) ) : dynamic_sidebar( 'banner_contactenos' ); endif; ?>
			</div>
		</div>
	</section>
<!-- End Banner -->